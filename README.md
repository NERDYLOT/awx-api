Role Name
=========

A brief description of the role goes here.

Requirements
------------
- python-pip
- pip install ansible-tower-cli ( on the tower node)

Role Variables
--------------


Dependencies
------------



Example Playbook
----------------

    - hosts: all
      username:
      password:
      tower_link:
      roles:
         - tower-api

License
-------

Apache

Author Information
------------------
Red Hat Consulting
